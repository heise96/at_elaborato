var mongo = require('mongodb');
const express = require('express');
const socket = require("socket.io");
const app = express();
const server = app.listen(process.env.PORT||8080);
const db = require('./server/mongodb');
const bodyParser = require('body-parser');
const verify = require('./server/verifyToken');
global.io = socket(server);


app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/client/'));

//POST METHODS
app.post('/register', (request, response) => {
  db.insertUser(request.body, res=>{
      if(res) response.status(200).end();
      else response.status(409).end();
    });
})

app.post('/login',(request,response)=>{
  db.login(request.body, (token, res, status)=>{
    switch (status){
      case 200:
        response.status(status);
        response.setHeader('auth-token', token);
        response.setHeader("Content-Type","application/json");
        response.write(JSON.stringify(res));
        response.end();
        break;
      default:
        response.status(status).end();
    }
  });
})

app.post('/reservation', (request,response)=>{
  db.postReservation(request.body, status=>{
    response.status(status).end();
  });
})

app.post('/updateSeatStatus', (request,response)=>{
  db.updateSeatStatus(request.body, _=>{
    response.status(200).end();
  });
})

//GET METHODS
app.get('/verifyTkn',verify, (request,response)=>{
  response.status(200).end();
})

app.get('/movie',(request,response)=>{
  db.getMovies(request, res=>{
    response.status(200);
    response.setHeader("Content-Type","application/json");
    response.write(JSON.stringify(res));
    response.end();
  });
})

app.get('/schedule',(request,response)=>{
  db.getSchedule(request, res=>{
    response.status(200);
    response.setHeader("Content-Type","application/json");
    response.write(JSON.stringify(res));
    response.end();
  });
})

app.get('/seats',(request,response)=>{
  db.getSeats(request, res=>{
    response.status(200);
    response.setHeader("Content-Type","application/json");
    response.write(JSON.stringify(res));
    response.end();
  });
})

app.get('/confirm',(request, response) =>{
  db.confirmEmail(request.query.email, _=>{
    response.redirect("./emailConfirmation.html");
  });
})

app.get('/userTicket',(request,response)=>{
  db.getUserTickets(request, res=>{
    response.status(200);
    response.setHeader("Content-Type","application/json");
    response.write(JSON.stringify(res));
    response.end();
  });
})

/*
app.get('/admin',()=>{
  db.addSeats();
})*/






