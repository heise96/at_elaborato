const jwt = require('jsonwebtoken');

module.exports = function (req,res,next){

    const token = req.headers["auth-token"];
    if(!token) {
        return res.status(401).send();
    }

    try{
        const verified = jwt.verify(token, "asdasdasd");
        next();

    }catch(err){
        return res.status(400).send();
    }
}