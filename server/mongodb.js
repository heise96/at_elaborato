var MongoClient = require("mongodb").MongoClient;
var bcrypt = require("./bcrypt");
var ObjectId = require("mongodb").ObjectID;
const jwt = require('jsonwebtoken');
const nodemailer = require('./config/nodemailer.config');
const Agenda = require('agenda');
require("../index");

//const connectionString =
  //unlo"mongodb+srv://m63Admin:middiff2020@cluster0.rugp4.mongodb.net/mydb?retryWrites=true&w=majority";

const connectionString = "mongodb://m63Admin:middiff2020@cluster0-shard-00-00.rugp4.mongodb.net:27017,cluster0-shard-00-01.rugp4.mongodb.net:27017,cluster0-shard-00-02.rugp4.mongodb.net:27017/cinemaDB?ssl=true&replicaSet=atlas-tabu6u-shard-0&authSource=admin&retryWrites=true&w=majority"

var db;
var clientMongo;
var usersCollection;
var moviesCollection;
var scheduleCollection;
var seatsCollection;
var reservationCollection;

MongoClient.connect(connectionString, { useUnifiedTopology: true }).then(
  (client) => {
    console.log("Connected to Database");

    db = client.db("cinemaDB");
    usersCollection = db.collection("users");
    moviesCollection = db.collection("movies");
    scheduleCollection = db.collection("schedule");
    seatsCollection = db.collection("seats");
    reservationCollection = db.collection("reservation");

    clientMongo = client;
  });

// LOGIN AND REGISTRATION

function isEmailUnique(email, callback) {
  var query = { email: email };
  usersCollection.find(query).toArray(function (err, res) {
    if (err) throw err;
    callback(res);
  });
}

//SEATS RESERVATION

function listenerOnSeats(request){
  
  const changeStream = seatsCollection.watch({
    $match: {
      "schedule" : ObjectId(request.query.schedule)
    }
  });  
  io.once('connection', function (socket) {
    socket.once('attachListener', function(){

      console.log('Connection!');

      changeStream.on('change', next=> {
        if(next.updateDescription != null){
          var res = {
            "seatsId" : next.documentKey._id,
            "seats" : next.updateDescription.updatedFields
          };
          socket.emit('seats',res);
        }
      });
    });
    socket.on('end', () => {
      socket.disconnect();
    });
  });
}

async function unlock(data, schedule, idUser) {

  const dbAgenda = await MongoClient.connect(connectionString,{ useUnifiedTopology: true });
  const agenda = new Agenda().mongo(dbAgenda.db("cinemaDB"));

  var randomString = Math.random().toString(36).slice(-8);
  var jobName = 'unlockJob-' + randomString;

  agenda.define(jobName,{maxConcurrency: 1000, priority: "high"},(job, done) => {
    job.attrs.data = data;

    var update_filter = {"status":{"$elemMatch": {"$elemMatch": {"$in": {}}}}};
    var update_string = {"$set": {}};
    update_string.$set[data] = "free";
    update_filter.status.$elemMatch.$elemMatch.$in[data] = [idUser];
    
    seatsCollection.findOneAndUpdate(
      {schedule: ObjectId(schedule)},
      update_string,update_filter,(err)=>{
        if(err) console.log(err);
        console.log("removing job...");
        done();
      });

      job.remove((err)=>{
        if (err == null) console.log("job removed");
        console.log(err);
      });
  });

  await new Promise(resolve => agenda.once('ready', resolve));

  agenda.schedule(new Date(Date.now() + 1000*600), jobName);
  agenda.start();
};

module.exports = { 

  // LOGIN AND REGISTRATION

  insertUser: function (user, callback) {

    isEmailUnique(user.email, function (res) {
      if (res.length == 0) {
        bcrypt.cryptPassword(user.password, function (err, hash) {
          if (err) throw err;
       
          var myobj = {
            name: user.name,
            surname: user.surname,
            email: user.email,
            password: hash,
            confirmed: false
          };

          usersCollection.insertOne(myobj, function (err) {
            if (err) throw err;
            nodemailer.sendConfirmationEmail(
              user.name,
              user.email
            );
            callback(true);
          });
        });
      } else {
        //Email already exists
        callback(false);
      }
    });
  },
  login : function(user, callback){

    var query = {email: user.email};

    usersCollection.find(query).toArray(function (err, res) {

      if (err) throw err; //TODO

      if(res.length == 0) callback(null, null, 404);
      else{
        if(res[0].confirmed == true){
          bcrypt.comparePassword(user.password, res[0].password, function(err, isPasswordMatch){
            if(err) throw err; //TODO
            if(isPasswordMatch){
              const token = jwt.sign({_id: res[0]._id}, "asdasdasd", {expiresIn: "30m"});

              callback(token, res[0], 200);

            } else callback(null, null, 401);
          })
        } else {
          //Email not confirmed
          callback(null, null, 401);
        }
      }
    });
  },
  confirmEmail : function(email, callback){
    var update_string = { "$set": {} };
    update_string.$set["confirmed"] = true;

    usersCollection.findOneAndUpdate({email: email}, update_string, (err, res) => {
      if (err) throw err; //TODO
      //Email confirmed
      callback();
    });
  },

  //MOVIES AND SCHEDULE

  getMovies : function (request, callback){

    moviesCollection.find().toArray(function (err, res) {
      if (err) throw err; //TODO
      callback(res);
    });
  },
  getSchedule: function(request, callback){
    var query = {"movie": request.query.movie, time: {$gte: new Date()}};

    scheduleCollection.find(query).toArray(function (err, res) {
      if (err) throw err; //TODO
      callback(res);
    });
  },

  //SEATS RESERVATION

  getSeats: function(request, callback){

    listenerOnSeats(request);

    var query ={"schedule": ObjectId(request.query.schedule)};
    seatsCollection.find(query).toArray(function (err, res) {
      if (err) throw err; //TODO
      callback(res[0]);
    });
  },
  updateSeatStatus: function(request, callback){

    var update_string = { "$set": {} };

    if (request.isSelected){
      update_string.$set["status."+request.row+"."+request.column] = request.user;
    }else update_string.$set["status."+request.row+"."+request.column] = "free";
    
    //TODO MATCH ON FREE
    seatsCollection.findOneAndUpdate(
      {schedule: ObjectId(request.schedule)},
      update_string,(err)=>{
        if(err) throw err; //TODO
        if(request.isSelected) unlock("status."+request.row+"."+request.column,request.schedule, request.user);
        callback();
      });
  },
  postReservation: async function(request,callback){

    const session = clientMongo.startSession();
    session.startTransaction();

    try{
      const opts = { session, returnOriginal: false};
      for(index in request.seat){

        let row = request.seat[index].row;
        let column = request.seat[index].column;
  
        var update_string = { "$set": {} };
        update_string.$set["status."+row+"."+column] = "booked";
  
        await seatsCollection.findOneAndUpdate(
          {schedule: ObjectId(request.schedule)},
          update_string,(err)=>{
            if(err) throw err //TODO
          }, opts);   
      }

      var userInfo = JSON.parse(request.user);

      var reservation = {
        "user" : ObjectId(userInfo._id),
        "schedule" : ObjectId(request.schedule),
        "seat" : request.seat
      };
      var idReservation;

      await reservationCollection.insertOne(reservation, function (err,reservationInserted) {
        if (err) throw err; //TODO
        idReservation = reservationInserted._id;
      }, {upsert: true}, opts);
      
      await session.commitTransaction().then(
        _ =>{
          nodemailer.sendTicketEmail(
            userInfo.name,
            userInfo.email,
            request.theater,
            request.time,
            request.seat,
            idReservation
          );
          callback(200);
        });
      
    }catch(err){
      console.log("ERROR IN TRANSACTION -> " + err);
      await session.abortTransaction();
      callback(500);
    }
    session.endSession();
  },

  //USER TICKET 

  getUserTickets: async function(request, callback){
    var tickets = [];
    var scheduleIDs = [];

    var query = {"user": ObjectId(request.query.userId)};

    reservationCollection.find(query).toArray(async function (err, reservationRes) {
      if (err) throw err; //TODO 

      for(index in reservationRes){
        scheduleIDs.push(ObjectId(reservationRes[index].schedule));

        var ticket = {
          "schedule" : reservationRes[index].schedule,
          "theater" : "",
          "time" : "",
          "title" : ""
        };
        tickets.push(ticket);
      }

      var filter = {_id : {'$in': scheduleIDs}};

      scheduleCollection.find(filter).toArray(function (err, resFromScheduleInfo){
        if (err) throw err; //TODO

        for(j in resFromScheduleInfo){
          //var i = tickets.findIndex(x => String(x.schedule) == String(resFromScheduleInfo[j]._id));
          var indexes = tickets.map((x,idx) => String(x.schedule) == String(resFromScheduleInfo[j]._id)? idx:'').filter(String);

          console.log(indexes);
          if(indexes!=-1){
            indexes.forEach(i=>{
              tickets[i].theater = resFromScheduleInfo[j].theater;
              tickets[i].time = resFromScheduleInfo[j].time;
              tickets[i].title = resFromScheduleInfo[j].movie;
            })
          }
        }
        callback(tickets);
      });
    });
  }

  /*
  addSeats : function(){
    
    var status_5 = [];
    var status_8 = [];
    var status_10 = [];

    for(var i=0; i<5; i++) {
      status_5[i] = [];
      for(var j=0; j<15; j++) {
        status_5[i][j] = "free";
      }
    }

    for(var i=0; i<8; i++) {
      status_8[i] = [];
      for(var j=0; j<15; j++) {
        status_8[i][j] = "free";
      }
    }

    for(var i=0; i<10; i++) {
      status_10[i] = [];
      for(var j=0; j<15; j++) {
        status_10[i][j] = "free";
      }
    }

    var item = [{
      schedule : ObjectId("601e6a7dc2f6037b405b9fdf"),
      price : 6.5,
      status : status_10
      },{

      schedule : ObjectId("601e6af10a686423e781a240"),
      price : 5.5,
      status : status_10
    }];


    seatsCollection.insert(item);
      
  }*/
};



