const nodemailer = require("nodemailer");
const config = require("./auth.config");
var fs = require('fs');
var handlebars = require('handlebars');

var readHTMLFile = (path, callback) => {
    fs.readFile(path, { encoding: 'utf-8' }, function (err, html) {
      if (err) {
        throw err;
      }
      else {
        return callback(null, html);
      }
    });
  };

const user = config.user;
const pass = config.pass;

const transport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: user,
    pass: pass,
  },
});

module.exports = {

    sendConfirmationEmail : function(name, email){
    console.log("nodemailer -> sending confimation email");

    readHTMLFile(__dirname + '/email_confirmation.html', function (err, html) {
        var template = handlebars.compile(html);
        var replacements = {
           name: name,
           url: "http://localhost:8080/confirm?email="+email
        };
        var htmlToSend = template(replacements);
        var mailOptions = {
            from: user,
            to : email,
            subject : 'Confirm your email',
            html : htmlToSend
        };
        transport.sendMail(mailOptions, (error, response)=> {
            if (error) {
                console.log(error);
                callback(error);
            }
        });
      });
    },
    sendTicketEmail : function(name, email, theater, time, seats, idReservation){
      console.log("nodemailer -> sending ticket email");

      var seatLabel = "";

      for(seat in seats){
        seatLabel += String.fromCharCode(65+Number(seats[seat].row)) + String(Number(seats[seat].column) + 1);
        console.log(seat + " " + seats.length);
        if(seat != seats.length-1) seatLabel += "-";
      }

      console.log(seatLabel);

      readHTMLFile(__dirname + '/email_ticket.html', function (err, html) {
        var template = handlebars.compile(html);
        var replacements = {
           name: name,
           theater: theater,
           time: time,
           seats: seatLabel
           //reservationID: idReservation
        };
        var htmlToSend = template(replacements);
        var mailOptions = {
            from: user,
            to : email,
            subject : 'Here your ticket!',
            html : htmlToSend
        };
        transport.sendMail(mailOptions, (error, response)=> {
            if (error) {
                console.log(error);
                callback(error);
            }
        });
      });


    }
}